const {
  RTMP_ENABLE,
  RTMP_PORT,
  RTMP_CHUNK_SIZE,
  RTMP_GOP_CACHE,
  RTMP_PING,
  RTMP_PING_TIMEOUT,
} = process.env;

module.exports =
  RTMP_ENABLE === "true"
    ? {
        port: RTMP_PORT,
        gop_cache: RTMP_GOP_CACHE,
        chunk_size: RTMP_CHUNK_SIZE,
        ping: RTMP_PING,
        ping_timeout: RTMP_PING_TIMEOUT,
      }
    : false;
