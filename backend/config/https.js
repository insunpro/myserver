const { HTTPS_ENABLE, HTTPS_PORT, HTTP_KEY, HTTP_CERT } = process.env;

module.exports =
  HTTPS_ENABLE === "true"
    ? {
        port: HTTPS_PORT,
        mediaroot: HTTP_KEY,
        webroot: HTTP_CERT,
      }
    : false;
