const {
  AUTH_ENABLE,
  AUTH_API,
  AUTH_API_USER,
  AUTH_API_PASS,
  AUTH_PLAY,
  AUTH_PUBLISH,
  AUTH_SECRET,
} = process.env;

module.exports =
  AUTH_ENABLE === "true"
    ? {
        api: AUTH_API,
        api_user: AUTH_API_USER,
        api_pass: AUTH_API_PASS,
        play: AUTH_PLAY,
        publish: AUTH_PUBLISH,
        secret: AUTH_SECRET,
      }
    : false;
