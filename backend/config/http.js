const {
  HTTP_ENABLE,
  HTTP_PORT,
  HTTP_MEDIA,
  HTTP_WEBROOT,
  HTTP_ALLOW_ORIGIN,
  HTTP_API,
  HTTP_STREAM,
} = process.env;

module.exports =
  HTTP_ENABLE === "true"
    ? {
        port: HTTP_PORT,
        mediaroot: HTTP_MEDIA,
        webroot: HTTP_WEBROOT,
        allow_origin: HTTP_ALLOW_ORIGIN,
        api: HTTP_API,
        stream: HTTP_STREAM,
      }
    : false;
