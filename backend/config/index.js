const auth = require("./auth");
const http = require("./http");
const rtmp = require("./rtmp");
const https = require("./https");

const config = {
  http,
  auth,
  rtmp,
  https,
};

console.log(config);

module.exports = config
